import lombok.SneakyThrows;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import ruzavin.manager.AppManager;

import java.io.File;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

public class TestInitializer {
	private static ChromeDriverService service;
	protected WebDriver driver;

	protected static AppManager appManager = new AppManager();

	@SneakyThrows
	@BeforeClass
	public static void setUp(){
		service = new ChromeDriverService.Builder()
				.usingDriverExecutable(new File("src/main/resources/chromedriver.exe"))
				.usingAnyFreePort()
				.build();
		service.start();

		appManager.setService(service);
	}

	@AfterClass
	public static void stopService(){
		service.stop();
	}

	@Before
	public void createDriver(){
		ChromeOptions chromeOptions = new ChromeOptions();
// Fixing 255 Error crashes
		chromeOptions.addArguments("--no-sandbox");
		chromeOptions.addArguments("--disable-dev-shm-usage");

		// Options to trick bot detection
		// Removing webdriver property
		chromeOptions.addArguments("--disable-blink-features=AutomationControlled");
		chromeOptions.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
		chromeOptions.setExperimentalOption("useAutomationExtension", null);

		// Changing the user agent / browser fingerprint
		chromeOptions.addArguments("window-size=1920,1080");
		chromeOptions.addArguments("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36");

		// Other
		chromeOptions.addArguments("disable-infobars");

		driver = new RemoteWebDriver(service.getUrl(), chromeOptions);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		appManager.setDriver(driver);
	}

	@After
	public void quitDriver(){
		appManager.quitDriver();
	}


}
