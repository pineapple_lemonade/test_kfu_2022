package ruzavin.helper;

import lombok.AllArgsConstructor;
import ruzavin.manager.AppManager;

@AllArgsConstructor
public class AbstractHelper {
	protected AppManager appManager;
}
