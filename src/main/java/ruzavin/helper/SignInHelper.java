package ruzavin.helper;

import org.openqa.selenium.WebElement;
import ruzavin.manager.AppManager;
import ruzavin.model.AccountData;

public class SignInHelper extends AbstractHelper{
	public SignInHelper(AppManager appManager) {
		super(appManager);
	}

	public void submitAccountData(AccountData data, WebElement email, WebElement password) {
		email.sendKeys(data.getEmail());
		password.sendKeys(data.getPassword());
	}
}
