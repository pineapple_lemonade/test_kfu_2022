package ruzavin.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ruzavin.manager.AppManager;
import ruzavin.model.AccountData;

public class SignInPage extends BaseNavigationPage {
	public SignInPage(WebDriver driver, AppManager appManager) {
		super(driver, appManager);
	}

	@FindBy(name = "email")
	private WebElement emailField;

	@FindBy(name = "password")
	private WebElement passwordField;

	@FindBy(css = ".lead:nth-child(9) > input")
	private WebElement submitButton;


	public AccountPage performAuth(AccountData data) {
		appManager.getSignInHelper().submitAccountData(data, emailField, passwordField);

		submitButton.click();

		return new AccountPage(driver, appManager);
	}
}
