package ruzavin.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ruzavin.manager.AppManager;

public class BaseNavigationPage extends PageObject{
	public BaseNavigationPage(WebDriver driver, AppManager appManager) {
		super(driver, appManager);
	}

	@FindBy(linkText = "Add Guide")
	private WebElement addGuideButton;

	public AddGuidePage goToAddGuidePage() {
		addGuideButton.click();

		return new AddGuidePage(driver, appManager);
	}


}
