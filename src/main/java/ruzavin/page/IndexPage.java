package ruzavin.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ruzavin.manager.AppManager;

public class IndexPage extends BaseNavigationPage {
	public IndexPage(WebDriver driver, AppManager appManager) {
		super(driver, appManager);
	}

	@FindBy(linkText = "Sign In")
	private WebElement signInButton;

	public SignInPage goToSignIn() {
		signInButton.click();

		return new SignInPage(driver, appManager);
	}


}
