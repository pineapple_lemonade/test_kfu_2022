package ruzavin.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import ruzavin.manager.AppManager;

public class PageObject {
	protected WebDriver driver;
	protected AppManager appManager;

	public PageObject(WebDriver driver, AppManager appManager) {
		this.driver = driver;
		this.appManager = appManager;
		PageFactory.initElements(driver, this);
	}
}
