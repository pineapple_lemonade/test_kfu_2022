package ruzavin.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ruzavin.manager.AppManager;
import ruzavin.model.AddGuideData;

public class AddGuidePage extends BaseNavigationPage {
	public AddGuidePage(WebDriver driver, AppManager appManager) {
		super(driver, appManager);
	}

	@FindBy(name = "title")
	private WebElement title;

	@FindBy(name = "text")
	private WebElement text;

	@FindBy(css = ".lead:nth-child(5) > input")
	private WebElement submitPage;

	public AddGuidePage addGuide(AddGuideData data) {
		appManager.getAddGuideHelper().insertData(data, title, text);
		submitPage.click();

		return this;
	}
}
