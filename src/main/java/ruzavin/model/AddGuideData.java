package ruzavin.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AddGuideData {
	private String title;
	private String text;
}
