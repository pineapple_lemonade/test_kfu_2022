package ruzavin.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class AccountData {
	private String email;
	private String password;
}
