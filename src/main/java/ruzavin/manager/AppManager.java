package ruzavin.manager;

import lombok.Data;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import ruzavin.helper.GuideHelper;
import ruzavin.helper.SignInHelper;

@Data
public class AppManager {
	private GuideHelper addGuideHelper;
	private SignInHelper signInHelper;
	private ChromeDriverService service;
	private WebDriver driver;

	public AppManager() {
		this.addGuideHelper = new GuideHelper(this);
		this.signInHelper = new SignInHelper(this);
	}

	public void quitDriver() {
		driver.quit();
	}

	public void openPage(String url) {
		driver.get(url);
		driver.manage().window().setSize(new Dimension(1536, 835));
	}
}
